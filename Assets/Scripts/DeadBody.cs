using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBody : MonoBehaviour
{
    public SpriteRenderer sr;
    public int id;
    public bool reported;
}
