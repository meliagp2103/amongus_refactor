using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{

    public List<GameObject> ImpostorStuff;

    private GameManager gm;

    void Start()
    {
        gm = ServiceLocator.Get<GameManager>();

        if (gm.datas[gm.localPlayer.id].role == Role.crewmate)
        {
            foreach(GameObject go in ImpostorStuff)
            {
                go.SetActive(false);
            }
        }
    }
}
