﻿using Mirror;
using System;
using System.Collections.Generic;
using UnityEngine;

public static class ServiceLocator
{
    private static Dictionary<Type, MonoBehaviour> Monos = new Dictionary<Type, MonoBehaviour>();
    private static Dictionary<Type, NetworkBehaviour> Networks = new Dictionary<Type, NetworkBehaviour>();
    private static Dictionary<Type, ScriptableObject> Settings = new Dictionary<Type, ScriptableObject>();
    private static Dictionary<Type, object> Datas = new Dictionary<Type, object>();

    #region MonoBehaviour Sharing
    public static void Share(this MonoBehaviour mono)
    {
        if (Monos.ContainsKey(mono.GetType()))
        {
            Debug.Log(mono.name + " already shared");
        }
        else
        {
            Monos.Add(mono.GetType(), mono);
        }
    }

    public static void Share(this NetworkBehaviour network)
    {
        if (Networks.ContainsKey(network.GetType()))
        {
            Debug.Log(network.name + " already shared");
        }
        else
        {
            Monos.Add(network.GetType(), network);
        }

    }

    public static void UnShare(this MonoBehaviour mono)
    {
        if (Monos.ContainsKey(mono.GetType()))
        {
            Monos.Remove(mono.GetType());
        }
    }
    #endregion

    #region ScriptableObject Sharing
    public static void Share(this ScriptableObject setting)
    {
        if (Settings.ContainsKey(setting.GetType()))
        {
            Debug.LogError(setting.name + " already shared");
        }
        else
        {
            Settings.Add(setting.GetType(), setting);
        }
    }

    public static void UnShare(this ScriptableObject setting)
    {
        if (Settings.ContainsKey(setting.GetType()))
        {
            Settings.Remove(setting.GetType());
        }
    }
    #endregion

    #region Data Sharing
    public static void Share(this object data)
    {
        if (Datas.ContainsKey(data.GetType()))
        {
            Datas[data.GetType()] = data;
        }
        else
        {
            Datas.Add(data.GetType(), data);
        }
    }
    #endregion

    public static T GetMono<T>() where T : MonoBehaviour => (T)Monos[typeof(T)];
    public static T Get<T>() where T : NetworkBehaviour => (T)Monos[typeof(T)];
    public static T GetSetting<T>() where T : ScriptableObject => (T)Settings[typeof(T)];
    public static T GetData<T>() => (T)Datas[typeof(T)];

    public static void Clear()
    {
        List<Type> zombies = new List<Type>();
        foreach (KeyValuePair<Type, MonoBehaviour> mono in Monos)
        {
            if (mono.Value == null) zombies.Add(mono.Key);
        }
        foreach (KeyValuePair<Type, NetworkBehaviour> network in Networks)
        {
            if (network.Value == null) zombies.Add(network.Key);
        }
        foreach (Type key in zombies)
        {
            Monos.Remove(key);
            Networks.Remove(key);
        }
    }
}