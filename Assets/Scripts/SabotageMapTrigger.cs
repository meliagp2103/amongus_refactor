using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SabotageMapTrigger : MonoBehaviour, IPointerClickHandler
{
    public SabotageBehaviour sabotage;
    private GameManager gm;
    private void Start()
    {
        gm = ServiceLocator.GetMono<GameManager>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        gm.localPlayer.Cmd_TriggerSabotage(sabotage.transform.GetSiblingIndex());
        gm.UI.Cooldown(gm.UI.sabotageButton, gm.UI.ToggleSabotage);
    }
}