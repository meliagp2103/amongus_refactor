using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class Keypad : MonoBehaviour
{
    private GameManager gm;
    public TextMeshProUGUI label;
    public TextMeshProUGUI codeLabel;
    public string code;
    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();
    }
    public void Open()
    {
        gameObject.SetActive(true);
        codeLabel.text = code;
    }
    public void Close()
    {
        gameObject.SetActive(false);
        label.text = "";
        label.color = Color.white;
    }
    public void Num_1()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}1";
        }
    }
    public void Num_2()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}2";
        }
    }
    public void Num_3()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}3";
        }
    }
    public void Num_4()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}4";
        }
    }
    public void Num_5()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}5";
        }
    }
    public void Num_6()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}6";
        }
    }
    public void Num_7()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}7";
        }
    }
    public void Num_8()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}8";
        }
    }
    public void Num_9()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}9";
        }
    }
    public void Num_0()
    {
        if (label.text.Length < 4)
        {
            label.text = $"{label.text}0";
        }
    }
    public void Cancel()
    {
        if (label.text.Length > 0)
        {
            label.text = label.text.Substring(0, label.text.Length - 1);
        }
    }
    public async void Confirm()
    {
        if(label.text == code)
        {
            label.color = Color.green;
            await Task.Delay(1000);
            if(gameObject!= null)
            {
                gm.taskManager.sabotageSelected.Complete();
            }
        }
        else
        {
            label.text = "";
        }
    }
}
