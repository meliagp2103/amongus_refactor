using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Player", menuName ="Settings/Player")]
public class PlayerSettings : ScriptableObject
{
    public Sprite skin;
    public Sprite ghost;
    public List<Color> colors = new List<Color>();
    public float speed = 5;
    public int minPlayers = 2;
    public int maxPlayers = 5;
    public int interactionRange = 2;
}
