using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public List<Transform> spawnPoints;
    public GameObject lobby;
    public GameObject map;
    private void Awake()
    {
        this.Share();
    }
    public void ShowMap()
    {
        lobby.SetActive(false);
        map.SetActive(true);
    }
    public Vector3 GetPosition(int i)
    {
        return spawnPoints[i].position;
    }
}
