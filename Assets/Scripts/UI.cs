using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;
using Random = UnityEngine.Random;

public class UI : MonoBehaviour
{
    public TextMeshProUGUI info;
    public GameObject play;
    public Button use;
    public GameObject crewmate;
    public GameObject impostor;
    public Button sabotageButton;
    public Button killButton;
    public Button reportCrewmateButton;
    public Button reportImpostorButton;
    private GameManager gm;
    public Image progressBar;
    public GameObject votePanel;
    public VoteButton voteButtonPrefab;


    const string pchKey_HostAddress = "hostAddressKey_GP2";
    const string pchKey_GameName = "AmongUs_EliaDilos_Key_GP2";
    const string pchValue_GameName = "AmongUs_EliaDilos_Value_GP2";

    private void Awake()
    {
        this.Share();
    }
    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();
        use.interactable = false;
    }

    public void ShowMessage(string text)
    {
        info.text = text;
    }
    public void ToggleReport(bool value)
    {
        reportCrewmateButton.interactable = value;
        reportCrewmateButton.image.color = value ? Color.green : Color.red;
        reportImpostorButton.interactable = value;
        reportImpostorButton.image.color = value ? Color.green : Color.red;
    }

    public void ToggleKill(bool value)
    {
        killButton.interactable = value;

        if(gm.localPlayer.idPlayerToKill > -1)
        {
            killButton.image.color = Color.green;
        }
        else
        {
            killButton.image.color = Color.red;
        }
    }

    public void TogglePlay(bool value)
    {
        play.SetActive(value);
    }

    public void ToggleUse(bool value)
    {
        use.interactable = value;
        use.image.color = value ? Color.green : Color.red;
    }

    public void ToggleSabotage(bool value)
    {
        sabotageButton.interactable = value;
        sabotageButton.image.color = value ? Color.green : Color.red;
    }

    public async void ShowMessage(string text, float time)
    {
        info.text = text;
        await Task.Delay(TimeSpan.FromSeconds(time));
        if (info != null) info.text = "";
    }

    public void ToggleHUD(Role role)
    {
        crewmate.SetActive(role == Role.crewmate);
        impostor.SetActive(role == Role.impostor);
    }

    public void Play()
    {
        gm.Countdown();
    }

    public void Kill()
    {
        gm.localPlayer.Cmd_KillPlayer(gm.localPlayer.idPlayerToKill);
        Cooldown(killButton, ToggleKill);
    }

    public void Use()
    {
        if (gm.taskManager.taskSelected != null)
        {
            gm.taskManager.taskSelected.minigame.Open(gm.taskManager.taskSelected.GetComponent<RectTransform>().anchoredPosition);
        }
        else
        {
            gm.taskManager.sabotageSelected.keypad.Open();
        }
        ToggleUse(false);
    }

    public void Report()
    {
        gm.localPlayer.Cmd_Report();
        ToggleReport(false);
    }

    public void Sabotage()
    {
        gm.localPlayer.Cmd_TriggerSabotage(Random.Range(0, gm.taskManager.sabotageContainer.childCount));
        Cooldown(sabotageButton, ToggleSabotage);
    }
    
    public async void Cooldown(Button btn, Action<bool> toggle)
    {
        toggle?.Invoke(false);
        float timer = 20;
        btn.image.fillAmount = 0;
        while (timer > 0)
        {
            Debug.Log("Cristo Kill Time: " + timer);
            timer--;
            btn.image.fillAmount = Mathf.InverseLerp(20, 0, timer);
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
        while (votePanel.activeSelf) await Task.Yield();
        toggle?.Invoke(true);
    }
   


    public void Exit()
    {
        if(!SteamLobbyManager.Initialized)
        {
            if (NetworkServer.active && NetworkClient.isConnected)
            {
                gm.ClearData();
                NetworkManager.singleton.StopHost();
                //SteamMatchmaking.LeaveLobby(SteamMatchmaking.GetLobbyData
            }
            // stop client if client-only
            else if (NetworkClient.isConnected)
            {
                NetworkManager.singleton.StopClient();
            }
        }
        else
        {
            if (NetworkServer.active && NetworkClient.isConnected)
            {
                gm.ClearData();
                ServiceLocator.GetMono<SteamLobbyManager>().LeaveLobby();
                NetworkManager.singleton.StopHost();
            }
            // stop client if client-only
            else if (NetworkClient.isConnected)
            {
                ServiceLocator.GetMono<SteamLobbyManager>().LeaveLobby();
                NetworkManager.singleton.StopClient();
            }
        }
    }
}