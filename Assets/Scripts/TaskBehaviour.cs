using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TaskBehaviour : MonoBehaviour
{
    public int id;

    public TaskLabel label;

    private GameManager gm;

    public Minigame minigame;

    private void Awake()
    {
        id = Random.Range(0, 100);
    }
    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();
    }
    public void Complete()
    {
        minigame.Close();
        label.Complete();
        gm.localPlayer.Cmd_AddTask();
        gameObject.SetActive(false);
    }

    public void Close()
    {
        minigame.Close();
    }

    //the player is entered in the Task Trigger Zone
    private void OnTriggerEnter2D(Collider2D collision)
    {
        gm.UI.ToggleUse(true);
        gm.taskManager.SetTask(this);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        gm.UI.ToggleUse(false);
        gm.taskManager.SetTask(null);
    }
    //when the button is pressed enable the minigame
}
