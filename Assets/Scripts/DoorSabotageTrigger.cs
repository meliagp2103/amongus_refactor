using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DoorSabotageTrigger : MonoBehaviour,IPointerClickHandler
{
    public DoorSabotageBehaviour door;
    private GameManager gm;

    private void Start()
    {
        gm = ServiceLocator.GetMono<GameManager>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Cristo");
        
        gm.localPlayer.Cmd_CloseDoor(door.transform.GetSiblingIndex());
    }
}
