using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public GameObject noSteam;
    public GameObject steam;

    private void Awake()
    {
        if(!FindObjectOfType<SteamLobbyManager>(true).enabled)
        {
            noSteam.SetActive(true);
            steam.SetActive(false);
        }
        else
        {
            noSteam.SetActive(false);
            steam.SetActive(true);
        }
    }
    public void Host() => NetworkManager.singleton.StartHost();
    public void Client() => NetworkManager.singleton.StartClient();
    public void AddressChange(string ip) => NetworkManager.singleton.networkAddress = ip;
}
