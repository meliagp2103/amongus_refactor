using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TaskLabel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI taskName;

    public void SetName(string name)
    {
        taskName.text = name;
    }
    public void Complete()
    {
        taskName.color = Color.green;
    }
}
