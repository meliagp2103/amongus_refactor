using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VoteButton : MonoBehaviour, IPointerClickHandler
{
    public int ID;
    private GameManager gm;
    public TextMeshProUGUI voteLabel;
    public Image player;
    public Image dead;
    private Button button;
    private void Awake()
    {
        button = GetComponent<Button>();
    }
    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();
        player.color = gm.datas[ID].color;
        UpdateVote();
    }
    private void OnEnable()
    {
        EventManager.Vote += UpdateVote;
    }
    private void OnDisable()
    {
        EventManager.Vote -= UpdateVote;
    }
    private void UpdateVote()
    {
        voteLabel.text = $"Player {ID}\nVotes: {gm.datas[ID].votes}";
    }

    public void SetDeadButton()
    {
        dead.enabled = true;
        voteLabel.gameObject.SetActive(false);
        button.interactable = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        gm.localPlayer.Cmd_VotePlayer(ID, gm.localPlayer.id);
    }
}