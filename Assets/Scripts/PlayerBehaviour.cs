using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class PlayerBehaviour : NetworkBehaviour
{
    public List<TaskBehaviour> tasksToDo;

    public GameObject killCollider;

    public DeadBody deadBody;

    public Minimap map;

    [HideInInspector] [SyncVar]
    public int idPlayerToKill = -1;
    [HideInInspector]
    [SyncVar]
    public int idPlayerToReport = -1;

    [SyncVar] public int id;

    [SerializeField] private Camera camera;
    private GameManager gm;
    private SpriteRenderer sr;
    private Vector3 dir;
    public int idVoted = -1;
    public GameObject taskPointer;
    private Transform nearestTask;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        map = FindObjectOfType<Minimap>(true);
    }
    private void SearchForNearestTask()
    {
        if (gm.taskManager.sabotageContainer.Cast<Transform>().Any(child => child.gameObject.activeSelf))
        {
            nearestTask = gm.taskManager.sabotageContainer.Cast<Transform>().First(task => task.gameObject.activeSelf);
            taskPointer.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
        {
            nearestTask = tasksToDo.Where(task => task.gameObject.activeSelf)
           .OrderBy(task => (task.transform.position - transform.position).sqrMagnitude).Select(task => task.transform).First();
            taskPointer.GetComponent<SpriteRenderer>().color = Color.white;
        }

        taskPointer.SetActive(true);

        Vector3 dir = nearestTask.transform.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        taskPointer.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        if ((nearestTask.transform.position - transform.position).sqrMagnitude <= 0.5f)
        {
            taskPointer.SetActive(false);
        }
    }
    private void OnEnable()
    {
        EventManager.MatchStart += OnMatchStart;
    }

    private void OnDisable()
    {
        EventManager.MatchStart -= OnMatchStart;
    }


    private void OnDestroy()
    {
        if (!gm.MinPlayersReached && NetworkServer.active)
        {
            gm.UI.TogglePlay(false);
        }
    }

    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();

        gm.AddPlayer(id);

        gm.players.Add(this);

        if (!isLocalPlayer)
        {
            camera.gameObject.SetActive(false);
        }
        else
        {
            gm.localPlayer = this;
        }

        sr.color = gm.datas[id].color;

        if (gm.MinPlayersReached && NetworkServer.active)
        {
            gm.UI.TogglePlay(true);
        }

        Cmd_SendMessage($"{gm.datas.Count} of {gm.playerSettings.maxPlayers}...");
    }

    private void OnMatchStart()
    {
        gm.datas[id].state = State.teleport;

        if (isLocalPlayer)
        {
            if(gm.datas[id].role == Role.impostor)
            {
                killCollider.SetActive(true);
                CircleCollider2D killColl = killCollider.GetComponent<CircleCollider2D>();
                killColl.radius = gm.playerSettings.interactionRange;
            }
        }

        Cmd_Move(gm.grid.GetPosition(id));
    }

    public override void OnStopClient()
    {
        base.OnStopClient();
        gm.RemovePlayer(id);
    }
    [Command]
    public void Cmd_KillPlayer(int id)
    {
        Rpc_KillPlayer(id);
 
    }
    [ClientRpc]
    public void Rpc_KillPlayer(int id)
    {
        idPlayerToKill = -1;
        idPlayerToReport = -1;
        gm.datas[id].state = State.dead;
        if(gm.datas.Count(data=> data.Value.role == Role.crewmate && data.Value.state != State.dead)==0)
        {
            Cmd_SendMessage("Impostor WIN!!!");
        }
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gm.UI.votePanel.activeSelf) return;
        if (isLocalPlayer)
        {
            PlayerBehaviour playerToKill = collision.GetComponent<PlayerBehaviour>();
            DeadBody dead = collision.GetComponent<DeadBody>();
            if (gm.datas[id].role == Role.impostor)
            {
                if(playerToKill != null && gm.datas[playerToKill.id].state != State.dead && gm.UI.killButton.image.fillAmount >= 1)
                {
                    idPlayerToKill = playerToKill.id;
                    Debug.Log("detected: "+idPlayerToKill);
                    gm.UI.ToggleKill(true);
                }
                if (dead != null)
                {
                    idPlayerToReport = dead.id;
                    gm.UI.ToggleReport(true);
                }
            }
            else
            {
               
                if (dead != null)
                {
                    idPlayerToReport = dead.id;
                    gm.UI.ToggleReport(true);
                }
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gm.UI.votePanel.activeSelf || !gm.datas.ContainsKey(id)) return;
        if (isLocalPlayer)
        {
            Debug.Log("exit");
            if (gm.datas[id].role == Role.impostor)
            {
                if(idPlayerToKill> -1)
                {
                    idPlayerToKill = -1;
                    gm.UI.ToggleKill(false);
                }
                else if(idPlayerToReport > -1)
                {
                    idPlayerToReport = -1;
                    gm.UI.ToggleReport(false);
                }
            }
            else
            {
                if(idPlayerToReport > -1)
                {
                    idPlayerToReport = -1;
                    gm.UI.ToggleReport(false);
                }
            }
        }
    }

    [ClientCallback]
    private void Update()
    {
        if ((gm.UI != null && gm.UI.votePanel.activeSelf) || !gm.datas.ContainsKey(id))
        {
            if(gm.taskManager.taskSelected != null && gm.taskManager.taskSelected.minigame.gameObject.activeSelf)
            {
                gm.taskManager.taskSelected.Close();
            }
            return;
        }
        if(isLocalPlayer && gm.datas[id].state == State.dead && gm.taskManager.taskSelected != null && gm.taskManager.taskSelected.minigame.gameObject.activeSelf)
        {
            gm.taskManager.taskSelected.Close();
        }
        //Debug.LogError("kicked count: "+ gm.datas.Count(data=> data.Value.state == State.kicked)+" "+ gm.datas.ContainsKey(id)+" "+ (gm.datas[id].state == State.kicked)+" "+( sr.sprite == gm.playerSettings.skin));
        if (gm.datas.ContainsKey(id) && gm.datas[id].state == State.kicked && sr.sprite == gm.playerSettings.skin)
        {
            Debug.LogError(id + " kicked");
            if (gm.datas[gm.localPlayer.id].state == State.kicked)
            {
                Debug.LogError(id + " i'm a ghost now");
                GetComponent<BoxCollider2D>().isTrigger = true;
                sr.sprite = gm.playerSettings.ghost;
                gm.UI.reportCrewmateButton.gameObject.SetActive(false);
            }
            else
            {
                GetComponent<BoxCollider2D>().isTrigger = true;
                Debug.LogError(id + " disappears");
                Debug.Log(id + " sprite null");
                sr.sprite = null;
            }
            return;
        }
        if (!deadBody.gameObject.activeSelf && gm.datas.ContainsKey(id) && !deadBody.reported)
        {
            if (gm.datas[id].state == State.dead)
            {
                GetComponent<BoxCollider2D>().isTrigger = true;
                Debug.Log(id + " dead");
                deadBody.id = id;
                deadBody.transform.SetParent(null);
                deadBody.gameObject.SetActive(true);
                deadBody.sr.color = sr.color;
                if (gm.datas[gm.localPlayer.id].state == State.dead)
                {
                    sr.sprite = gm.playerSettings.ghost;
                    gm.UI.reportCrewmateButton.gameObject.SetActive(false);
                }
                else
                {
                    Debug.Log(id + " sprite null");
                    sr.sprite = null;
                }

            }
        }

        if (!isLocalPlayer || gm.datas[id].state == State.teleport) return;
        if (Input.GetKeyDown(KeyCode.M) && gm.datas[id].state == State.play)
        {
            map.gameObject.SetActive(!map.gameObject.activeSelf);
        }
        //if(gm.datas[id].role == Role.impostor && map.gameObject.activeSelf && gm.datas[id].state == State.play)
        //{

        //}
        if (gm.datas[id].state == State.play && gm.datas[id].role == Role.crewmate) SearchForNearestTask();
        dir = Vector3.zero;
            dir.x = Input.GetAxis("Horizontal");
            dir.y = Input.GetAxis("Vertical");
            if (dir.sqrMagnitude > 0)
            {
                Cmd_Move(transform.position + dir.normalized * gm.datas[id].speed * (!NetworkServer.active ? 1 : 1) * Time.deltaTime);
            }
        
    }


    [Command]
    public void Cmd_SendMessage(string message)
    {
        gm.Rpc_ShowMessage(message);
    }


    [Command]
    public void Cmd_TriggerSabotage(int i)
    {
        Debug.Log("Cmd trigger");
        gm.taskManager.codesInserted = 0;
        gm.taskManager.Countdown();
        gm.taskManager.Rpc_TriggerSabotage(i);
        Rpc_ToggleImpostorStuff(false);
    }

    [ClientRpc]
    public void Rpc_ToggleImpostorStuff(bool isActive)
    {
        foreach(var go in map.ImpostorStuff)
        {
            go.SetActive(isActive);
        }
    }

    [Command]
    public void Cmd_AddTask()
    {
        //Rpc_AddTask();
        gm.taskManager.tasksDone++;
    }

   [Command]
   public void Cmd_Report()
    {
        Rpc_Report();
    }

    [ClientRpc] public void Rpc_Report()
    {
        idPlayerToKill = -1;
        idPlayerToReport = -1;
        if(gm.datas[id].state == State.play)
        {
            gm.CountdownVoting();
            gm.UI.votePanel.SetActive(true);
            var players = gm.datas.Where(data => data.Value.state != State.kicked).ToDictionary(p => p.Key, p => p.Value);
            for (int i = 0; i < players.Count; i++)
            {
                var voteButton = Instantiate(gm.UI.voteButtonPrefab, gm.UI.votePanel.transform);
                voteButton.ID = i;
                if (players[i].state == State.dead)
                {
                    voteButton.SetDeadButton();
                }
            }
        }
        FindObjectsOfType<DeadBody>(true).ToList().ForEach(dead =>
        {
            dead.reported = true;
            dead.gameObject.SetActive(false);
        });
    }

    [ClientRpc] public void Rpc_EndVote()
    {
        gm.UI.votePanel.gameObject.SetActive(false);
        foreach (Transform child in gm.UI.votePanel.transform)
        {
            Destroy(child.gameObject);
        }
        foreach(var data in gm.datas)
        {
            data.Value.votes = 0;
        }
    }

    [Command]
    public void Cmd_VotePlayer(int id, int voter)
    {

        Rpc_VotePlayer(id, voter);
    }

    [ClientRpc]
    public void Rpc_VotePlayer(int id, int voter)
    {
        if(voter == this.id)
        {
            if (idVoted > -1)
            {
                gm.datas[idVoted].votes--;
                Debug.Log(idVoted + " vote --");
            }
            gm.datas[id].votes++;
            Debug.Log(id + " vote ++");
            idVoted = id;
            EventManager.Vote?.Invoke();
        }  
    }

    [Command]
    public void Cmd_InsertCode()
    {
        Debug.Log("CMD codeInserted++");
        Rpc_InsertCode();
    }

    [Command]
    public void Cmd_Move(Vector3 position)
    {
        Rpc_Move(position);
    }

    [ClientRpc]
    public void Rpc_InsertCode()
    {
        Debug.Log("RPC codeInserted++");
        gm.taskManager.codesInserted++;
    }

    [ClientRpc] public void Rpc_Move(Vector3 position)
    {
        transform.position = position;

        if(gm.datas[id].state == State.teleport)
        {
            gm.datas[id].state = State.play;
            //Cmd_SendMessage("");
            gm.UI.TogglePlay(false);
            if(isLocalPlayer && gm.datas[id].role == Role.crewmate)
            tasksToDo = gm.taskManager.GetTask();
        }
    }

    [Command]
    public void Cmd_CloseDoor(int id)
    {
        gm.taskManager.Rpc_ToggleDoor(id, true);
        gm.taskManager.CountDownDoor(id);
        Rpc_ToggleImpostorStuff(false);
    }

}
