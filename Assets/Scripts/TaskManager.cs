using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class TaskManager : NetworkBehaviour
{
    public Transform taskContainer;

    public Transform doorContainer;

    public TaskLabel taskLabelPrefab;

    public int taskAmountToExtract;

    private GameManager gm;

    [SyncVar]
    public float TotalTasks;

    [SyncVar(hook = nameof(UpdateTask))]
    public float tasksDone;

    public Transform sabotageContainer;

    public int totalCodes;

    [SyncVar]
    public int codesInserted = 0;

    [Command]
    public void Cmd_TriggerSabotage(int i)
    {
        Debug.Log("cmd sabotage");
        Rpc_TriggerSabotage(i);
    }

    [ClientRpc]
    public void Rpc_TriggerSabotage(int i)
    {
        Debug.Log("rpc sabotage");
        sabotageContainer.GetChild(i).gameObject.SetActive(true);
    }

    [Server]
    public async void Countdown()
    {
        float timer = 30;
        Debug.Log("start countdown");
        while (timer >= 0 && codesInserted < totalCodes && !gm.UI.votePanel.activeSelf)
        {
            gm.Rpc_ShowMessage($"{timer} left... | {codesInserted} / {totalCodes}");
            timer--;
            await Task.Delay(TimeSpan.FromSeconds(1));
            if (codesInserted == totalCodes) break;
        }
        if (codesInserted == totalCodes || gm.UI.votePanel.activeSelf)
        {
            gm.Rpc_ShowMessage($"");
        }
        else
        {
            gm.Rpc_ShowMessage($"Impostor WIN!");
        }

        gm.localPlayer.Rpc_ToggleImpostorStuff(true);
    }

    [Server]
    public async void CountDownDoor(int id)
    {
        float timer = doorContainer.GetChild(id).GetComponent<DoorSabotageBehaviour>().amountForCooldown;

        while (timer >= 0)
        {
            timer--;
            await Task.Delay(TimeSpan.FromSeconds(1));
        }

        Rpc_ToggleDoor(id,false);
        gm.localPlayer.Rpc_ToggleImpostorStuff(true);
    }

    public void UpdateTask(float oldValue, float newValue)
    {
        Debug.Log(Progresss);
        gm.UI.progressBar.fillAmount = Progresss;
        if (gm.UI.progressBar.fillAmount >= 1)
        {
            gm.localPlayer.Cmd_SendMessage("Crewmates WIN!");
        }
    }

    private float Progresss => tasksDone / TotalTasks;

    public List<TaskBehaviour> taskPool;
    public TaskBehaviour taskSelected;
    public SabotageBehaviour sabotageSelected;


    public void SetTask(TaskBehaviour task)
    {
        taskSelected = task;
    }
    public void SetSabotage(SabotageBehaviour sabotage)
    {
        sabotageSelected = sabotage;
    }

    private void Awake()
    {
        this.Share();
    }
    void Start()
    {
        taskPool = taskPool.OrderBy(task => task.id).ToList();
        gm = ServiceLocator.Get<GameManager>();
    }

    public List<TaskBehaviour> GetTask()
    {
        List<TaskBehaviour> tasks = new List<TaskBehaviour>();
        for(int i = 0; i< taskPool.Count; i++)
        {
            if(i< taskAmountToExtract)
            {
                tasks.Add(taskPool[i]);
            }
            else
            {
                taskPool[i].gameObject.SetActive(false);
            }
        }
        foreach(TaskBehaviour task in tasks)
        {
            var label = Instantiate(taskLabelPrefab, taskContainer);
            label.SetName(task.name);
            task.label = label;
        }
        return tasks;
    }

    [ClientRpc]
    public void Rpc_ToggleDoor(int id,bool isActive)
    {
        Debug.Log("Cristo2");
        doorContainer.GetChild(id).gameObject.SetActive(isActive);
    }

}