using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class WirePoint : MonoBehaviour
{
    private Minigame minigame;
    private SpriteRenderer sr;

    private void Awake()
    {
        minigame = GetComponentInParent<Minigame>();
        sr = GetComponent<SpriteRenderer>();
    }
    Vector3 worldMousePos => new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);


    private void OnMouseDrag()
    {
        if(minigame.starters.Contains(sr))
        {
            minigame.lines[transform.GetSiblingIndex()].startColor =sr.color;
            minigame.lines[transform.GetSiblingIndex()].endColor = sr.color;
            minigame.lines[transform.GetSiblingIndex()].positionCount = 2;
            minigame.lines[transform.GetSiblingIndex()].SetPosition(0, transform.position);
            minigame.lines[transform.GetSiblingIndex()].SetPosition(1, new Vector3( Input.mousePosition.x, Input.mousePosition.y,1));
        }
    }
    private async void OnMouseUp()
    {
        if (minigame.starters.Contains(sr))
        {
            RaycastHit2D hit = Physics2D.Linecast(minigame.lines[transform.GetSiblingIndex()].GetPosition(0), minigame.lines[transform.GetSiblingIndex()].GetPosition(1), 1 << LayerMask.NameToLayer("WireTarget"));
            
            if (hit.collider != null && hit.collider.GetComponent<SpriteRenderer>().color == sr.color)
            {
                minigame.lines[transform.GetSiblingIndex()].positionCount = 2;
                minigame.lines[transform.GetSiblingIndex()].SetPosition(0, transform.position);
                minigame.lines[transform.GetSiblingIndex()].SetPosition(1, new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
                minigame.starters.Remove(sr);
                if (minigame.starters.Count == 0)
                {
                    await Task.Delay(1000);
                    if(minigame!=null)
                    minigame.gm.taskManager.taskSelected.Complete();
                }
            }
            else
            {
                if (minigame.lines[transform.GetSiblingIndex()].positionCount > 0)
                {
                    minigame.lines[transform.GetSiblingIndex()].SetPosition(0, Vector3.zero);
                    minigame.lines[transform.GetSiblingIndex()].SetPosition(1, Vector3.zero);
                    minigame.lines[transform.GetSiblingIndex()].positionCount = 0;
                }

            }
        }
    }
}
