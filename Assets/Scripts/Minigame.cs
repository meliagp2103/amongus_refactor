using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Minigame : MonoBehaviour
{
    public GameManager gm;
   [HideInInspector]  public List<LineRenderer> lines;
   [HideInInspector] public List<SpriteRenderer> starters;
    [SerializeField] private Transform starterContainer;
    [SerializeField] private Transform linesContainer;
    private List<Color32> colors;
    public void Open(Vector3 pos)
    {
       transform.position = pos;
        gameObject.SetActive(true);
        starters = starterContainer.Cast<Transform>().Select(start => start.GetComponent<SpriteRenderer>()).ToList();
        lines = linesContainer.Cast<Transform>().Select(line => line.GetComponent<LineRenderer>()).ToList();
        Debug.Log(starters.Count+" "+lines.Count);
        colors = new List<Color32>() { new Color32(255, 0, 0, 255), new Color32(255, 255, 0, 255), new Color32(0, 0, 255, 255), new Color32(255, 0, 255, 255) };
        for (int i = 0; i<starters.Count;i++)
        {
            int randomIndex = Random.Range(0, colors.Count);
            starters[i].color = colors[randomIndex];
            colors.RemoveAt(randomIndex);
            if (lines[i].positionCount > 0)
            {
                lines[i].SetPosition(0, Vector3.zero);
               lines[i].SetPosition(1, Vector3.zero);
               lines[i].positionCount = 0;
            }
        }
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();
    }

}
