using Mirror;
using Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamLobbyManager : SteamManager
{
   public Callback<LobbyCreated_t> lobbyCreated;
    public Callback<GameLobbyJoinRequested_t> gameLobbyJoinRequest;
    public Callback<LobbyEnter_t> gameLobbyEntered;
    public Callback<LobbyMatchList_t> gameLobbyListRequest;

    const string pchKey_HostAddress = "hostAddressKey_GP2";
    const string pchKey_GameName = "AmongUs_EliaDilos_Key_GP2";
    const string pchValue_GameName = "AmongUs_EliaDilos_Value_GP2";

    private string pchKey_Password;
    private string pchValue_Password;


    private CSteamID currentSteamSession;

    protected override void Awake()
    {
        if (!Initialized)
        {
            base.Awake();
            this.Share();
        }
    }

    private void Start()
    {
        lobbyCreated = Callback<LobbyCreated_t>.Create(OnLobbyCreated);
        gameLobbyJoinRequest = Callback<GameLobbyJoinRequested_t>.Create(OnLobbyJoinRequest);
        gameLobbyEntered = Callback<LobbyEnter_t>.Create(OnGameLobbyEntered);
        gameLobbyListRequest = Callback<LobbyMatchList_t>.Create(OnLobbyListRequest);
    }

    private void OnLobbyListRequest(LobbyMatchList_t pCallback)
    {
        CSteamID lobby;
        for(int i = 0; i < pCallback.m_nLobbiesMatching; i++)
        {
            lobby = SteamMatchmaking.GetLobbyByIndex(i);
            if (lobby.IsValid())
            {
                SteamMatchmaking.JoinLobby(lobby);
                return;
            }
        }
        CreateLobby(ELobbyType.k_ELobbyTypePublic);
    }

    private void OnGameLobbyEntered(LobbyEnter_t pCallback)
    {
        if(NetworkServer.active)
        {
            return;
        }
        string hostAddress = SteamMatchmaking.GetLobbyData
            (currentSteamSession = new CSteamID(pCallback.m_ulSteamIDLobby),
            pchKey_HostAddress
            );
        NetworkManager.singleton.networkAddress = hostAddress;
        NetworkManager.singleton.StartClient();
    }

    private void OnLobbyJoinRequest(GameLobbyJoinRequested_t pCallback)
    {
        SteamMatchmaking.JoinLobby(pCallback.m_steamIDLobby);
    }

    private void OnLobbyCreated(LobbyCreated_t pCallback)
    {
        if (pCallback.m_eResult != EResult.k_EResultOK)
        {
            return;
        }
        SteamMatchmaking.SetLobbyData
            (new CSteamID(pCallback.m_ulSteamIDLobby),
            pchKey_HostAddress,
            SteamUser.GetSteamID().ToString());

        SteamMatchmaking.SetLobbyData
          (new CSteamID(pCallback.m_ulSteamIDLobby),
          pchKey_GameName,
         pchValue_GameName);

        SteamMatchmaking.SetLobbyData
            (new CSteamID(pCallback.m_ulSteamIDLobby),
            pchKey_Password,
            pchValue_Password);

        NetworkManager.singleton.StartHost();
    }

    public void CreateLobby(ELobbyType lobbyType)
    {
        SteamMatchmaking.CreateLobby(lobbyType, NetworkManager.singleton.maxConnections);
    }

    public void QuickPlay()
    {
        SteamMatchmaking.AddRequestLobbyListStringFilter(
            pchKey_GameName,
            pchValue_GameName,
            ELobbyComparison.k_ELobbyComparisonEqual
            );
        SteamMatchmaking.RequestLobbyList();
    }

    public void HostPrivate()
    {
        CreateLobby( ELobbyType.k_ELobbyTypePublic);
    }

    public void JoinPrivate()
    {
        SteamMatchmaking.AddRequestLobbyListStringFilter(
              pchKey_GameName,
              pchValue_GameName,
              ELobbyComparison.k_ELobbyComparisonEqual
              );
        SteamMatchmaking.AddRequestLobbyListStringFilter(
             pchKey_Password,
             pchValue_Password,
             ELobbyComparison.k_ELobbyComparisonEqual
             );
        SteamMatchmaking.RequestLobbyList();
    }
    public void PasswordChange(string code)
    {
        pchKey_Password = code;
        pchValue_Password = code;
    }

    public void LeaveLobby()
    {
        SteamMatchmaking.LeaveLobby(currentSteamSession);

    }

}