using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;
using System;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : NetworkBehaviour
{
    public PlayerSettings playerSettings;

    public readonly SyncDictionary<int, PlayerData> datas = new SyncDictionary<int, PlayerData>();

    public SyncList<Color> currentColors = new SyncList<Color>();

    public List<PlayerBehaviour> players = new List<PlayerBehaviour>();

    public UI UI;
    public GridManager grid;
    public TaskManager taskManager;
    public PlayerBehaviour localPlayer;

    public bool MinPlayersReached => datas.Count >= playerSettings.minPlayers;

    private void Awake()
    {
        this.Share();
        currentColors = new SyncList<Color>(playerSettings.colors);
    }

    private void OnDisable()
    {
        SceneManager.sceneUnloaded += (Scene scene) => ServiceLocator.Clear();
    }

    private void Start()
    {
        UI = ServiceLocator.GetMono<UI>();
        grid = ServiceLocator.GetMono<GridManager>();
        taskManager = ServiceLocator.Get<TaskManager>();
    }

    private Color GetRandomColor()
    {
        int i = UnityEngine.Random.Range(0, currentColors.Count);
        Color temp = currentColors[i];
        currentColors.RemoveAt(i);
        return temp;
    }

    [Server]
    public async void Countdown()
    {
        float timer = 5;
        while (timer >= 0)
        {
            Rpc_ShowMessage($"{timer} to start...");
            timer--;
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
        AssignRandomRole();
        Rpc_ShowMap();
    }

    [Server]
    public async void CountdownVoting()
    {
        float timer = 20;
        Debug.Log("start countdown");
        while (timer >= 0)
        {
           Rpc_ShowMessage($"{timer} to end voting...");
            timer--;
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
        if (datas.Count(data => data.Value.votes > 0) == 0 || datas.Where(data => data.Value.votes == datas.Max(data => data.Value.votes)).Count() > 1)
        {
            Debug.Log("no one voted or pari");
            Rpc_ShowMessage($"");
            localPlayer.Rpc_EndVote();
        }
        else
        {
            var playerVoted = datas.Single(data => data.Value.votes == datas.Max(data => data.Value.votes));
            KickPlayer(playerVoted.Key);
            if(playerVoted.Value.role == Role.crewmate)
            {
                Rpc_ShowTempMessage($"Player " + playerVoted.Key + " kicked", 3);
            }
            else
            {
                Rpc_ShowTempMessage($"Player " + playerVoted.Key + " was the impostor!",3);
            }
            //EventManager.KickPlayer?.Invoke(datas.First(data => data.Value.votes == datas.Max(data => data.Value.votes)).Key);
            Debug.Log("player most voted: " + datas.First(data => data.Value.votes == datas.Max(data => data.Value.votes)).Key + " with " + datas.Max(data => data.Value.votes) + " votes");
            localPlayer.Rpc_EndVote();
        }
    }

    [ClientRpc] private void KickPlayer(int id)
    {
        datas[id].state = State.kicked;
    }

    //[Server]
    //public async void CountdownVotation()
    //{
    //    float timer = 20;
    //    while (timer >= 0)
    //    {
    //        Rpc_ShowMessage($"{timer} to end voting...");
    //        timer--;
    //        await Task.Delay(TimeSpan.FromSeconds(1));
    //    }
    //    Debug.Log("player most voted: " + datas.First(data => data.Value.votes == datas.Max(data => data.Value.votes)).Key+" with "+ datas.Max(data => data.Value.votes)+" votes");
    //    EventManager.KillPlayer?.Invoke(datas.First(data => data.Value.votes == datas.Max(data => data.Value.votes)).Key);
    //    //RemovePlayer(datas.First(data => data.Value.votes == datas.Max(data => data.Value.votes)).Key);
    //}
   

  

    [ClientRpc]
     private void Rpc_ShowMap()
     {
        UI.ShowMessage($"You are {datas[localPlayer.id].role}!", 3);
        UI.ToggleHUD(datas[localPlayer.id].role);

        EventManager.MatchStart?.Invoke();
        grid.ShowMap();
     }

    private void AssignRandomRole()
    {
        int randomIndex = Random.Range(0, datas.Count);
        AssignImpostor(randomIndex);
        //Debug.Log(randomIndex +" is the impostor");
    }

    [ClientRpc] private void AssignImpostor(int id)
    {
        datas[id].role = Role.impostor;
    }

    [Server]
    public void AddPlayer(int id)
    {
        datas.Add(id, new PlayerData
        {
            state = State.lobby,
            role = Role.crewmate,
            speed = playerSettings.speed,
            color = GetRandomColor(),
            votes = 0
        });
    }

    [Server]
    public void RemovePlayer(int id)
    {
        if (datas.ContainsKey(id))
        {
            currentColors.Add(datas[id].color);
            if (datas[id].state == State.lobby)
            {
                localPlayer.Cmd_SendMessage($"{datas.Count - 1} of {playerSettings.maxPlayers}...");
            }
            datas.Remove(id);
        }
        if(datas.Count(data => data.Value.role == Role.crewmate) == 0)
        {
            Rpc_ShowMessage($"Impostor WIN!");
        }
    }


    [Server]
    public void ClearData()
    {
        foreach(var data in datas)
        {
            currentColors.Add(data.Value.color);
        }
        datas.Clear();
    }


    [ClientRpc]
    public void Rpc_ShowMessage(string text)
    {
        UI.ShowMessage(text);
    }

    [ClientRpc]
    public void Rpc_ShowTempMessage(string text, float time)
    {
        UI.ShowMessage(text, time);
    }

}

public class PlayerData
{
    public State state;
    public Role role;
    public float speed;
    public Color color;
    public int votes;
}
public enum Role { crewmate, impostor }
public enum State { lobby, teleport, play, dead, kicked }