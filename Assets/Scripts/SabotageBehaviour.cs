using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SabotageBehaviour : MonoBehaviour
{
    private GameManager gm;
    public Keypad keypad;
    private void Start()
    {
        gm = ServiceLocator.Get<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        gm.UI.ToggleUse(true);
        gm.taskManager.SetSabotage(this);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        gm.UI.ToggleUse(false);
        gm.taskManager.SetSabotage(null);
    }
    public void Complete()
    {
        keypad.Close();
        gm.localPlayer.Cmd_InsertCode();
        Debug.Log("complete");
        gameObject.SetActive(false);
    }
}
